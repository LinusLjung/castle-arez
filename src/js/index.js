import * as states from 'states';
import Phaser from 'phaser';

Phaser.Game.prototype.scheduleDestroy = function (object) {
	this.scheduledDestroys.push(object);
};
Phaser.Game.prototype.handleScheduledDestroys = function () {
	var i;
	var iL = this.scheduledDestroys.length;

	if (iL > 0) {
		for (i = 0; i < iL; i++) {
			this.scheduledDestroys[i].destroy();
		}
	}
};
Phaser.Game.prototype.scheduledDestroys = [];

const game = new Phaser.Game(1300, 600, Phaser.AUTO, '');

game.state.add("loading", states.Loading);
game.state.add("level1", states.Level1);
// game.state.add("level2", Level2);
// game.state.add("level3", Level3);
// game.state.add("level4", Level4);
// game.state.add("level5", Level5);
// game.state.add("level6", Level6);
// game.state.add("level7", Level7);
// game.state.add("level8", Level8);
// game.state.add("level9", Level9);
// game.state.add("level10", Level10);
// game.state.add("level11", Level11);
// game.state.add("level12", Level12);
// game.state.add("level13", Level13);
// game.state.add("level14", Level14);
game.state.add("level15", states.Level15);
game.state.add("over", states.Over);

window.game = game;

game.state.start("loading");
