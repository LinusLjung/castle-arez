import Tiles from 'Tiles';

function Ladder(game, x, y, width, height, tileScale) {
	Phaser.TileSprite.call(this, game, x, y, width, height, "castlearez", Tiles.ladder.index - 1);

	game.physics.arcade.enable(this);

	this.body.immovable = true;

	this.anchor.setTo(.5, 1);
	this.tileScale.setTo(((typeof tileScale === "number") ? tileScale : 1));
	this.body.width /= 6;
}

Ladder.prototype = Object.create(Phaser.TileSprite.prototype);
Ladder.prototype.constructor = Ladder;

export default Ladder;
