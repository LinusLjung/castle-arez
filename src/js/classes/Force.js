function Force(force, degrees) {
	this.degrees = degrees - 90;
	this.radians = this.degrees * Math.PI / 180;
	this.force = force;

	this.x = Math.cos(this.radians) * this.force;
	this.y = Math.sin(this.radians) * this.force;
}

export default Force;
