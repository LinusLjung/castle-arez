function ShootingTrap(game, x, y, sprite, frame, vertical, speed, frequency, weapon, delay) {
	Phaser.Sprite.call(this, game, x, y, sprite, frame);

	game.physics.arcade.enable(this);
	this.body.immovable = true;

	this.anchor.y = 0;

	this.vertical = vertical;
	this.speed = speed;
	this.bounds = frequency;
	this.weapon = weapon;

	if (typeof delay === "undefined") {
		delay = 0;
	}

	window.setTimeout(function () {
		window.setInterval(this.shoot.bind(this), frequency);
	}.bind(this), delay);
}

ShootingTrap.prototype = Object.create(Phaser.Sprite.prototype);
ShootingTrap.prototype.constructor = ShootingTrap;

ShootingTrap.prototype.shoot = function () {
	var projectile = this.weapon.group.getFirstExists(false);

	if (projectile !== null) {
		this.weapon.playSound();

		projectile.direction = "up";
		projectile.scale.y = 1;
		projectile.reset(this.body.position.x, this.body.position.y + 16);

		if (this.vertical) {
			projectile.body.velocity.y = this.speed;
		} else {
			projectile.body.velocity.x = this.speed;
		}
	}
};

export default ShootingTrap;
