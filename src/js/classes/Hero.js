import Arez from 'Arez';
import Tiles from 'Tiles';
import Sounds from 'Sounds';
import Inventory from 'Inventory';
import Force from 'Force';
import Ghost from 'Ghost';
import Collider from 'Collider';
import Updater from 'Updater';
import AnimationManager from 'AnimationManager';
import Info from 'Info';
import Unit from 'Unit';

var cursors;

function Hero(game, x, y, atlas, sprite) {
	Phaser.Sprite.call(this, game, x, y, atlas, sprite);

	cursors = Arez.cursors;

	this.originalX = x;
	this.originalY = y;
	this.sprite = sprite;

	this.inventory = new Inventory();

	this.normal();

	this.body.maxVelocity.x = this.maxVelocity = 150;

	this.anchor.x = .5;
	this.anchor.y = 1;

	this.tile = {
		index: 0
	};

	this.initAnimationManager();

	this.direction = "right";

	this.damageForce = new Force(300, 45);
}

Hero.prototype = Object.create(Phaser.Sprite.prototype);
Hero.prototype.constructor = Hero;

Hero.prototype.update = function () {
	if (this.isTouchingGround()) {
		if (this.flying) {
			this.normal();
			game.input.keyboard.reset(false);
			game.input.gamepad.reset(false);
		}

		if (this.pushedBack) {
			this.recover();
			game.input.keyboard.enabled = true;
			game.input.keyboard.reset(false);
			game.input.gamepad.enabled = true;
			game.input.gamepad.reset(false);
		}
	}

	if (this.alive && !this.flying && !this.pushedBack) {
		if (this.climbing) {
			if (cursors.up.isDown || cursors.w.isDown || Arez.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_UP)) {
				this.body.velocity.y = -150;
				this.animationManager.play("climbUp", true);
				if (!this.checkForLadder()) {
					this.unclimb();
				}
			} else if (cursors.down.isDown || cursors.s.isDown || Arez.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN)) {
				this.body.velocity.y = 150;
				this.animationManager.play("climbDown", true);
				if (this.isTouchingGround()) {
					this.unclimb();
					this.animationManager.stop("climbDown");
				}
			} else if (cursors.right.isDown || cursors.d.isDown || cursors.left.isDown || cursors.a.isDown || Arez.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) || Arez.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT)) {
				this.unclimb();
			} else {
				this.body.velocity.setTo(0);
				this.animationManager.stop("climbUp", false);
				this.animationManager.stop("climbDown", false);
			}
		} else {
			if (cursors.left.isDown || cursors.a.isDown || Arez.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT)) {
				this.goLeft();
			} else if (cursors.right.isDown || cursors.d.isDown || Arez.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT)) {
				this.goRight();
			} else {
				this.standStill();
			}
		}

		if (!this.isTouchingGround() && !this.climbing) {
			if (this.body.velocity.y < 0) {
				this.animationManager.play("fly");
			} else {
				this.animationManager.play("fall");
			}
		} else {
			this.animationManager.stop("falling");
		}
	}
};

Hero.prototype.onOutOfBounds = function () {
	var ghost = new Ghost(this);

	game.add.existing(ghost);
	game.camera.follow(ghost);
	game.scheduleDestroy(this);

	game.world.moveDown(ghost);
};

Hero.prototype.die = function () {
	this.alive = false;

	this.unregisterEvents();

	Collider.removeForSprite(this);

	this.body.velocity.x = ((this.direction === "right") ? -this.damageForce.x : this.damageForce.x);
	this.body.velocity.y = this.damageForce.y;

	this.animationManager.play("die");
	Sounds.get("wilhelm").play();

	return false;
};

Hero.prototype.onLayerCollision = function (hero, tile) {
	if (tile.index === Tiles.water.index || tile.index === Tiles.spear.index || tile.index === Tiles.lava.index || tile.index === Tiles.speard.index) {
		this.die();
	}

	if (this.isTouchingGround()) {
		this.tile = tile;
	}
};

Hero.prototype.loot = function (itemObject) {
	this.inventory.add(itemObject.getLoot());
	itemObject.destroy();
	Arez.currentLevel.themeSound.pause();
	Sounds.get("loot").play();

	return false;
};

Hero.prototype.goLeft = function () {
	this.body.velocity.x -= (Tiles.getFriction(this.tile.index));

	if (this.direction === "right") {
		this.direction = "left";
		this.scale.x *= -1;
	}

	this.animationManager.play("run");
};

Hero.prototype.goRight = function () {
	this.body.velocity.x += (Tiles.getFriction(this.tile.index));

	if (this.direction === "left") {
		this.direction = "right";
		this.scale.x *= -1;
	}

	this.animationManager.play("run");
};

Hero.prototype.standStill = function () {
	if (this.body.velocity.x !== 0 && this.isTouchingGround() && typeof this.tile !== "undefined") {
		if (this.body.velocity.x < 0) {
			if (this.body.velocity.x > -(Tiles.getFriction(this.tile.index))) {
				this.body.velocity.x = 0;
			} else {
				this.body.velocity.x += Tiles.getFriction(this.tile.index);
			}
		} else {
			if (this.body.velocity.x < Tiles.getFriction(this.tile.index)) {
				this.body.velocity.x = 0;
			} else {
				this.body.velocity.x -= Tiles.getFriction(this.tile.index);
			}
		}
	}

	//Sit
	if ((cursors.down.isDown || cursors.s.isDown || Arez.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN)) && !this.climbing) {
		this.sitting = true;
		this.animationManager.play("sit");
	} else {
		this.sitting = false;
		this.animationManager.stop(1);
		this.animationManager.play("idle");
	}
};

Hero.prototype.startJump = function () {
	if (!this.checkForLadder(true)) {
		if (this.isTouchingGround()) {
			this.animationManager.play("jump");
		}
	}
};

Hero.prototype.jump = function () {
	if (this.isTouchingGround()) {
		this.body.velocity.y = -410;
	}
};

Hero.prototype.isTouchingGround = function () {
	return (this.body.onFloor() || this.body.touching.down);
};

Hero.prototype.attack = function () {
	var weapon;

	if (this.inventory.hasWeaponEquipped()) {
		weapon = this.inventory.equipped;

		if (game.time.now > weapon.readyTime && weapon.quantity > 0) {
			if (weapon.subType === "throw") {
				this.throw(weapon);
			} else if (weapon.subType === "melee") {
				this.melee(weapon);
			}
		}
	}
};

Hero.prototype.melee = function (weapon) {
	weapon.sprite.attack(this);
};

Hero.prototype.throw = function (weapon) {
	var projectile = weapon.group.getFirstExists(false);

	if (projectile !== null) {
		this.onThrowComplete = function () {
			weapon.playSound();

			projectile.reset(0, 0);

			if (this.sitting) {
				projectile.position.y = this.y + 51 - this.height;
			} else {
				projectile.position.y = this.y + 20 - this.height;
			}

			if (this.direction === "right") {
				projectile.position.x = this.x + 35;

				if (projectile.direction === "left") {
					projectile.scale.x *= -1;
					projectile.direction = "right";
				}

				projectile.body.velocity.x = 400;
			} else {
				projectile.position.x = this.x - 35;

				if (projectile.direction === "right") {
					projectile.scale.x *= -1;
					projectile.direction = "left";
				}

				projectile.body.velocity.x = -400;
			}

			weapon.decrement();
			weapon.readyTime = game.time.now + weapon.cooldown;
		};

		this.animationManager.play(weapon.subType, this.onThrowComplete);
	}
};

Hero.prototype.unregisterEvents = function () {
	game.input.keyboard.enabled = false;
	cursors.up.onDown.removeAll();
	cursors.w.onDown.removeAll();
	Arez.keys.spacebar.onDown.removeAll();
	Arez.keys.q.onDown.removeAll();
	Arez.keys.e.onDown.removeAll();

	if (this.gamepad) {
		game.input.gamepad.enabled = false;
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_DPAD_UP).onDown.removeAll();
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_A).onDown.removeAll();
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_X).onDown.removeAll();
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_LEFT_BUMPER).onDown.removeAll();
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_RIGHT_BUMPER).onDown.removeAll();
	}
};

Hero.prototype.registerEvents = function () {
	game.input.keyboard.enabled = true;
	cursors.up.onDown.add(this.startJump, this);
	cursors.w.onDown.add(this.startJump, this);
	Arez.keys.spacebar.onDown.add(this.attack, this);
	Arez.keys.q.onDown.add(this.inventory.previous, this.inventory);
	Arez.keys.e.onDown.add(this.inventory.next, this.inventory);
};

Hero.prototype.registerGamepad = function () {
	if (typeof Arez.gamepad !== "undefined" && Arez.gamepad.connected) {
		this.gamepad = true;
		game.input.gamepad.enabled = true;
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_DPAD_UP).onDown.add(this.startJump, this);
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_A).onDown.add(this.jump, this);
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_X).onDown.add(this.attack, this);
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_LEFT_BUMPER).onDown.add(this.inventory.previous, this.inventory);
		Arez.gamepad.getButton(Phaser.Gamepad.XBOX360_RIGHT_BUMPER).onDown.add(this.inventory.next, this.inventory);
	}
};

Hero.prototype.enablePhysics = function () {
	game.physics.arcade.enable(this);

	this.checkWorldBounds = true;
	this.events.onOutOfBounds.add(this.onOutOfBounds.bind(this));
	this.body.collideWorldBounds = true;
};

Hero.prototype.unclimb = function () {
	this.climbing = false;
	this.body.gravity.y = 800;
};

Hero.prototype.climb = function (hero, ladder) {
	this.position.x = ladder.position.x;
	this.climbing = true;
	this.body.gravity.setTo(0);
	this.body.velocity.setTo(0);
};

Hero.prototype.normal = function () {
	this.enablePhysics();
	this.registerEvents();
	this.registerGamepad();

	this.visible = true;
	this.flying = false;
	this.climbing = false;
	this.invulnerable = false;
	this.pushedBack = false;
	this.body.gravity.y = 800;
	this.body.maxVelocity.x = this.maxVelocity;
	game.input.keyboard.enabled = true;
};

Hero.prototype.fly = function () {
	this.flying = true;
	this.body.maxVelocity.x = 999999;
	game.input.keyboard.enabled = false;
	this.animationManager.stop(1);
};

Hero.prototype.checkForLadder = function (callCallback) {
	return game.physics.arcade.overlap(this, Arez.ladders, ((callCallback === true) ? this.climb : null), null, this);
};

Hero.prototype.updateSprite = function () {
	Phaser.Sprite.call(this, game, this.originalX, this.originalY, this.sprite);
	this.initAnimationManager();
};

Hero.prototype.initAnimationManager = function () {
	this.animationManager = new AnimationManager(this);
	this.animationManager.add("idle", Phaser.Animation.generateFrameNames("idle", 1, 22), 10, true, 0);
	this.animationManager.add("run", Phaser.Animation.generateFrameNames("running", 1, 27), 50, true, 0);
	this.animationManager.add("jump", Phaser.Animation.generateFrameNames("jumping", 1, 5), 50, false, 2, this.jump);
	this.animationManager.add("fly", ["jumping6"], 1, true, 2);
	this.animationManager.add("fall", Phaser.Animation.generateFrameNames("jumping", 9, 14), 50, false, 2, function () { this.animationManager.play("falling", null); });
	this.animationManager.add("falling", ["jumping15"], 50, true, 2, null, ["fall"]);
	this.animationManager.add("climbUp", Phaser.Animation.generateFrameNames("climbing", 1, 8), 15, true, 2);
	this.animationManager.add("climbDown", Phaser.Animation.generateFrameNames("climbing", 8, 1), 15, true, 2);
	this.animationManager.add("sit", Phaser.Animation.generateFrameNames("sitting", 4, 1), 30, false, 0, function () { this.animationManager.play("sitting"); });
	this.animationManager.add("sitting", "sitting1", null, null, 0, null, ["sit"]);
	this.animationManager.add("throw", Phaser.Animation.generateFrameNames("throwing", 1, 4), 20, false, 3);
	this.animationManager.add("die", Phaser.Animation.generateFrameNames("dying", 1, 5), 25, false, 10, function () { this.animationManager.play("dying"); });
	this.animationManager.add("dying", Phaser.Animation.generateFrameNames("dying", 6, 8).concat("dying7"), 25, false, 10);
	this.animationManager.add("knockback", ["dying1"], 1, true, 9);
	this.animationManager.setDefaultAnimation("idle");
};

Hero.prototype.onTakeDamage = function () {
	var timer;

	this.unclimb();
	this.healthInfo.setText(this.hp);

	if (this.hp > 0) {
		timer = game.time.create(true);

		this.position.y -= 1;

		timer.add(1, function () {
			this.body.velocity.x = ((this.direction === "right") ? -this.damageForce.x : this.damageForce.x);
			this.body.velocity.y = this.damageForce.y;

			game.input.keyboard.enabled = false;
			game.input.gamepad.enabled = false;

			this.pushedBack = true;

			Collider.disableCollision("heroEnemies");

			Sounds.get("ouch").play();

			this.animationManager.play("knockback");

			window.setTimeout(function () {
				this.invulnerable = false;
				this.recover();
			}.bind(this), 1000);
		}, this);

		timer.start();
	}
};

Hero.prototype.recover = function () {
	this.pushedBack = false;
	this.animationManager.stop("knockback");
	Collider.enableCollision("heroEnemies");
};

Hero.prototype.newHealthInfo = function () {
	this.healthInfo = new Info(game.add.sprite(null, null, "castlearez", Tiles.plus.index - 1), this.hp, 0, 0);
};

Arez.extend(Hero.prototype, Unit);

export default Hero;
