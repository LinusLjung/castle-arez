import Arez from 'Arez';
import Sounds from 'Sounds';
import Updater from 'Updater';
import Collider from 'Collider';

function SwingWeapon(game, x, y, key, frame) {
  Phaser.Sprite.call(this, game, x, y, key, frame);
}

SwingWeapon.prototype = Object.create(Phaser.Sprite.prototype);
SwingWeapon.prototype.constructor = SwingWeapon;

SwingWeapon.prototype.attack = function (attacker) {
	var tween;
	var rightAngle = 260;
	var leftAngle = 130;

	if (!this.attacking) {
		this.attacking = true;
		Sounds.get("swoosh").play();

    this.victims = [];

		Updater.add("swordAttack", function () {
			if (attacker.direction === "right") {
				if (this.direction === "left") {
					this.scale.x *= -1;
					this.direction = "right";
				}

				this.position.x = attacker.position.x + 16;
			} else {
				if (this.direction === "right") {
					this.scale.x *= -1;
					this.direction = "left";
				}
				this.position.x = attacker.position.x - 16;
			}

			this.position.y = attacker.position.y - attacker.height;
		}, this);

		Collider.addCollision("swordGoblin", {
			first: this,
			second: Arez.currentLevel.goblins,
			checkCollision: function (sword, goblin) {
        this.victims.push(goblin);

				goblin.die();
				return false;
			}
		});

    Collider.addCollision("destroyables", {
      first: this,
      second: Arez.currentLevel.destroyables,
      checkCollision: function (sword, destroyable) {
        if (this.victims.indexOf(destroyable) === -1) {
          this.victims.push(destroyable);
          destroyable.takeDamage();
        }

        return false;
      },
      context: this
    });

		game.physics.arcade.enable(this);
		this.angle = ((attacker.direction === "right") ? rightAngle : leftAngle);
		this.position.y = attacker.position.y;
		this.visible = true;

		tween = game.add.tween(this).to({
			//angle: (sprite.direction === "right") ? sprite.angle + 165 : sprite.angle + (360 - 165),
			/*angle: sprite.angle + 165,
			y: sprite.y + 30*/
		}, 200, Phaser.Easing.Linear.None, true);

		tween.onUpdateCallback(function (tween, progress) {
			this.position.y = attacker.position.y - attacker.height + (30 * progress);
			this.angle = ((this.direction === "right") ? (260 + (165 * progress)) : (130 - ((360 - 165) * progress)));
		}, this);

		tween.onComplete.add(function () {
			this.attacking = false;
			this.visible = false;
			this.body = null;
			Updater.remove("swordAttack");
      Collider.removeCollision("swordGoblin");
      Collider.removeCollision("destroyables");
		}, this);
	}
};

export default SwingWeapon;
