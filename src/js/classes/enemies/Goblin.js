import Sounds from 'Sounds';
import Arez from 'Arez';
import Ghost from 'Ghost';
import Unit from 'Unit';

function Goblin(game, x, y, sprite) {
	Phaser.Sprite.call(this, game, x, y, sprite);

	game.physics.arcade.enable(this);

	this.anchor.x = .5;
	this.anchor.y = 1;
	this.animations.add("left", [3, 4, 5], 10, true);
	this.animations.add("right", [6, 7, 8], 10, true);

	this.direction = "right";
	this.speed = 175;
	this.animations.play("right");

	this.body.velocity.x = this.speed;

	this.patrol = {
		left: 950,
		right: 1500
	};
}

Goblin.prototype = Object.create(Phaser.Sprite.prototype);
Goblin.prototype.constructor = Goblin;

Goblin.prototype.update = function () {
	if (this.position.x < this.patrol.left) {
		this.direction = "right";
		this.body.velocity.x = 175;
		this.animations.play("right");
	} else if (this.position.x > this.patrol.right) {
		this.direction = "left";
		this.body.velocity.x = -175;
		this.animations.play("left");
	}
};

Goblin.prototype.goLeft = function () {
	if (this.direction === "right") {
		this.body.scale.x *= -1;
	}

	this.body.velocity.x = -this.speed;
};

Goblin.prototype.goRight = function () {
	if (this.direction === "left") {
		this.body.scale.x *= -1;
	}

	this.body.velocity.x = this.speed;
};

Goblin.prototype.die = function () {
	game.add.existing(new Ghost(this));
	game.scheduleDestroy(this);
	Sounds.get("goblinDies").play();
};

Arez.extend(Goblin.prototype, Unit);

export default Goblin;
