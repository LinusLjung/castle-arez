import AnimationManager from 'AnimationManager';
import Arez from 'Arez';
import Unit from 'Unit';

function Bat(game, x, y, atlas, sprite) {
	Phaser.Sprite.call(this, game, x, y, atlas, sprite);

	game.physics.arcade.enable(this);

	this.anchor.y = 1;

	this.direction = "left";
	this.scale.x = -1;
	this.speed = -175;

	this.checkWorldBounds = true;

	this.animationManager = new AnimationManager(this);
	this.animationManager.add("fly", Phaser.Animation.generateFrameNames("flying", 1, 6).concat(Phaser.Animation.generateFrameNames("flying", 5, 2)), 8, true, 0);
	this.animationManager.setDefaultAnimation("fly");

	this.animationManager.play("fly");

	this.anchor.y = .5;

	this.body.velocity.x = this.speed;
}

Bat.prototype = Object.create(Phaser.Sprite.prototype);
Bat.prototype.constructor = Bat;

Bat.prototype.setX = function (x) {
	this.originalX = x;
};

Bat.prototype.setY = function (y) {
	this.originalY = y;
};

Bat.prototype.update = function () {
	this.sin = Math.sin(this.position.x / 100) * 4 * 32;
	this.position.y = this.originalY + this.sin - (2 * 32);
	this.angle = Math.atan((32 / 25) * Math.cos(this.position.x / 100)) * 180 / Math.PI; //y = kx + m -> y = k. rad2deg = rad * 180 / pi
};

Bat.prototype.onSpawn = function () {
	this.events.onEnterBounds.addOnce(function () {
		this.events.onOutOfBounds.addOnce(this.kill, this);
	}, this);

	this.reset(this.originalX, this.originalY);

	this.body.velocity.x = this.speed;
};

Arez.extend(Bat.prototype, Unit);

export default Bat;
