/* jshint -W008 */
Arez.classes.push(function (window, namespace, undefined) {
	"use strict";

	function Dragon(game, x, y, atlas, sprite) {
		Phaser.Sprite.call(this, game, x, y, atlas, sprite);

		game.physics.arcade.enable(this);

		this.anchor.y = 1;

		this.direction = "left";
		this.scale.x = -1;
		this.speed = -175;

		this.checkWorldBounds = true;
		this.events.onOutOfBounds.add(this.die, this);

		this.animationManager = new Arez.AnimationManager(this);
		this.animationManager.add("fly", Phaser.Animation.generateFrameNames("flying", 1, 8), 8, true, 0);
		this.animationManager.setDefaultAnimation("fly");

		this.animationManager.play("fly");

		this.anchor.y = .5;

		this.body.velocity.x = this.speed;
	}

	Dragon.prototype = Object.create(Phaser.Sprite.prototype);
	Dragon.prototype.constructor = Dragon;

	Dragon.prototype.update = function () {
	};

	Dragon.prototype.die = function () {
		this.kill();
	};

	Dragon.prototype.onSpawn = function () {
		this.body.velocity.x = this.speed;
	};

	namespace.Dragon = Dragon;
});
