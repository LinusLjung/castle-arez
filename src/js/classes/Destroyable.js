import Collider from 'Collider';
import Arez from 'Arez';
import Sounds from 'Sounds';

var incrementor = 0;

function Destroyable(x, y, frames, destroySoundName) {
	Phaser.Sprite.call(this, game, x, y, "castlearez", frames[0]);

	game.physics.arcade.enable(this);

	this.body.immovable = true;

	this.index = 0;
	this.frames = frames;
	this.destroySoundName = destroySoundName;
	this.id = incrementor++;

	Collider.addCollision("destroyableCollideHero" + this.id, {
		first: Arez.hero,
		second: this
	});
}

Destroyable.prototype = Object.create(Phaser.Sprite.prototype);
Destroyable.prototype.constructor = Destroyable;

Destroyable.prototype.takeDamage = function () {
	this.index++;

	if (this.index >= this.frames.length) {
		this.die();
	} else {
		this.frame = this.frames[this.index];
	}

	return true;
};

Destroyable.prototype.die = function () {
	game.scheduleDestroy(this);

	Collider.removeCollision("destroyableCollideHero" + this.id);

	Sounds.get(this.destroySoundName).play();

	if (typeof this.onDestroy === "function") {
		this.onDestroy.apply(window, arguments);
	}

	return false;
};

export default Destroyable;
