var Updater = {
	objects: {},

	add: function (key, object, context) {
		Updater.objects[key] = {
			object: object,
			context: context,
			arguments: Array.prototype.slice.call(arguments, 3)
		};
	},

	remove: function (key) {
		delete Updater.objects[key];
	},

	update: function () {
		var key;
		var object;

		for (key in Updater.objects) {
			if (Updater.objects.hasOwnProperty(key)) {
				object = Updater.objects[key];

				if (typeof object.object === "function") {
					object.object.apply(Updater.objects[key].context, Updater.objects[key].arguments);
				} else {
					object.object.update();
				}
			}
		}
	},

	clear: function () {
		Updater.objects = {};
	}
};

export default Updater;
