var Collider = {
	collisions: {},
	disabled: [],

	checkCollisions: function () {
		var id;
		var collision;

		for (id in Collider.collisions) {
			if (Collider.collisions.hasOwnProperty(id) && Collider.disabled.indexOf(id) === -1) {
				collision = Collider.collisions[id];

				if (game.physics.arcade[collision.method](collision.first, collision.second, collision.onCollision, collision.checkCollision, collision.context)) {
					if (collision.once) {
						delete Collider.collisions[id];
					}
				}
			}
		}
	},

	addCollision: function (id, collision) {
		if (collision.overlap) {
			collision.method = "overlap";
		} else {
			collision.method = "collide";
		}

		Collider.collisions[id] = collision;
	},

	removeCollision: function (id) {
		delete Collider.collisions[id];
	},

	removeForSprite: function (sprite) {
		var key;

		for (key in this.collisions) {
			if (this.collisions.hasOwnProperty(key)) {
				if (this.collisions[key].first === sprite ||this.collisions[key].second === sprite) {
					this.removeCollision(key);
				}
			}
		}
	},

	disableCollision: function (id) {
		if (Collider.disabled.indexOf(id) === -1) {
			Collider.disabled.push(id);
		}
	},

	enableCollision: function (id) {
		var index = Collider.disabled.indexOf(id);

		if (index !== -1) {
			Collider.disabled.splice(index, 1);
		}
	},

	clear: function () {
		Collider.collisions = {};
	}
};

export default Collider;
