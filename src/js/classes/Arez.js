import Blood from 'Blood';
import Sounds from 'Sounds';

var Arez = {
  extend: function (destination, source, force) {
		var key;

		if (typeof force === "undefined") {
			force = false;
		}

		for (key in source) {
			if (source.hasOwnProperty(key) && (force || !destination.hasOwnProperty(key))) {
				destination[key] = source[key];
			}
		}
	},

	arezGameOver: function () {
		Arez.currentLevel.themeSound.stop();
		Sounds.get("wilhelm").play();
		game.state.start("over");
	},

	collideHeroEnemy: function (hero, enemy) {
		hero.takeDamage(enemy.getDamage());

		return false;
	},

	collideDuckLayer: function (duck) {
		duck.kill();

		new Blood(duck);
	},

	collideGoblinProjectile: function (goblin, projectile) {
		projectile.kill();
		goblin.die();
		return false;
	},

	collideSwordGoblin: function (sword, goblin) {
		goblin.die();
		return false;
	},

	collideHeroFallTrap: function (hero, fallTrap) {
		if (hero.body.touching.down) {
			fallTrap.fall();
		}
	},

	collideFireballTrap: function (fireball) {
		if (fireball.direction === "down") {
			fireball.kill();
		}

		return false;
	}
};

export default Arez;
