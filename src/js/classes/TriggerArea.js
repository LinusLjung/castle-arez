function TriggerArea(x, y, width, height, once) {
	this.rectangle = new Phaser.Rectangle(x, y, width, height, once);

	this.once = ((typeof once === "boolean") ? once : true);
	this.triggerers = [];
	this.triggered = false;
}

TriggerArea.prototype.update = function () {
	var i;
	var triggerer;

	if (this.once && this.triggered) {
		return;
	}

	for (i = 0; i < this.triggerers.length; i++) {
		triggerer = this.triggerers[i];

		if (Phaser.Rectangle.intersectsRaw(this.rectangle, triggerer.left, triggerer.right, triggerer.top, triggerer.bottom)) {
			if (typeof this.onTrigger === "function") {
				this.onTrigger.call(this, triggerer);
			}
		}
	}
};

TriggerArea.prototype.addTriggerer = function (triggerer) {
	this.triggerers.push(triggerer);
};

export default TriggerArea;
