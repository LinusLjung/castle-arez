var defaultFriction = 20;
var Tiles = {
  water: {
    index: 1
  },
  hidden: {
    index: 4
  },
  grey: {
    index: 5
  },
  lava: {
    index: 6
  },
  tree: {
    index: 7
  },
  purpleLeaf: {
    index: 8
  },
  secret: {
    index: 10
  },
  cobblestone: {
    index: 35
  },
  movingPlatformRed: {
    index: 38
  },
  movingPlatformBlue: {
    index: 39
  },
  grass: {
    index: 41
  },
  fallTrap: {
    index: 42
  },
  dirt: {
    index: 43
  },
  dark: {
    index: 44
  },
  speard: {
    index: 45
  },
  spear: {
    index: 47
  },
  ladder: {
    index: 48
  },
  arrow: {
    index: 51
  },
  box: {
    index: 52
  },
  log1: {
    index: 58
  },
  log2: {
    index: 59
  },
  log3: {
    index: 60
  },
  log4: {
    index: 61
  },
  fireball: {
    index: 69,
  },
  arez: {
    index: 71
  },
  duckProjectile: {
    index: 72
  },
  duckIcon: {
    index: 73
  },
  duckStationary: {
    index: 74
  },
  ghost: {
    index: 75
  },
  sword: {
    index: 76
  },
  plus: {
    index: 77
  },
  boxCracked: {
    index: 78
  }
};

Tiles.getFriction = function (index) {
  var tile;
  var found = false;

  for (tile in this) {
    if (this.hasOwnProperty(tile)) {
      if (index === this[tile].index) {
        found = true;
        break;
      }
    }
  }

  if (found) {
    return ((typeof this[tile].friction === "undefined") ? defaultFriction : this[tile].friction);
  } else {
    return defaultFriction;
  }
};
Tiles.getFriction.bind(Tiles);

export default Tiles;
