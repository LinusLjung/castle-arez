function FallTrap(game, x, y, width, height, sprite, frame, immovable, timeout) {
	Phaser.TileSprite.call(this, game, x, y, width, height, sprite, frame);

	this.physicsType = Phaser.SPRITE;
	game.physics.arcade.enable(this);
	this.checkWorldBounds = true;
	this.body.immovable = immovable;
	this.events.onOutOfBounds.add(this.die, this);

	this.timeout = timeout;

	this.falling = false;
}

FallTrap.prototype = Object.create(Phaser.Sprite.prototype);
FallTrap.prototype.constructor = FallTrap;

FallTrap.prototype.update = function () {
};

FallTrap.prototype.die = function () {
	game.scheduleDestroy(this);
};

FallTrap.prototype.fall = function () {
	if (!this.falling) {
		if (typeof this.timeout === "undefined") {
			this.body.immovable = false;
		} else {
			window.setTimeout((function () {
				this.body.gravity.y = 200;
			}).bind(this), this.timeout * 1000);
		}

		this.falling = true;
	}
};

export default FallTrap;
