function Blood(unit) {
	Phaser.Sprite.call(this, game, unit.position.x, unit.position.y, "blood");

	this.anchor = unit.anchor;

	game.add.existing(this);

	window.setTimeout((function () {
		game.scheduleDestroy(this);
	}).bind(this), 1000);
}

Blood.prototype = Object.create(Phaser.Sprite.prototype);
Blood.prototype.constructor = Blood;

Blood.prototype.update = function () {
};

export default Blood;
