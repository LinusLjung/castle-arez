import Info from 'Info';

function Inventory() {
	this.inventory = [];
}

Inventory.prototype.add = function (item) {
	this.inventory.push(item);
	item.inventory = this;

	if (typeof this.equipped === "undefined") {
		this.equip(item);
	}
};

Inventory.prototype.equip = function (item) {
	this.equipped = item;
	this.newEquipInfo();
};

Inventory.prototype.hasWeaponEquipped = function () {
	return ((typeof this.equipped !== "undefined") && (this.equipped.type === "weapon"));
};

Inventory.prototype.newEquipInfo = function (force) {
	if (typeof this.equipped !== "undefined") {
		if (typeof this.equipInfo === "undefined" || force === true) {
			this.equipInfo = new Info(this.equipped.icon, this.equipped.quantity, game.width - (3 * 32), 0);
		} else {
			this.equipInfo.setText(this.equipped.quantity);
			this.equipInfo.setIcon(this.equipped.icon);
		}
	}
};

Inventory.prototype.next = function () {
	var currentIndex = 0;

	if (this.inventory.length > 0) {
		if (typeof this.equipped !== "undefined") {
			currentIndex = this.inventory.indexOf(this.equipped);
		}

		this.equip(this.inventory[((currentIndex + 1) % this.inventory.length)]);
	}
};

Inventory.prototype.previous = function () {
	var previousIndex = 0;

	if (this.inventory.length > 0) {
		if (typeof this.equipped !== "undefined") {
			previousIndex = this.inventory.indexOf(this.equipped) - 1;
		}

		if (previousIndex === -1) {
			previousIndex = this.inventory.length - 1;
		}

		this.equip(this.inventory[previousIndex]);
	}
};

Inventory.prototype.updateEquipText = function () {
	this.equipInfo.setText(this.equipped.quantity);
};

export default Inventory;
