function Info(icon, text, x, y) {
	this.background = game.add.graphics(0, 0).beginFill(0x00).drawRect(x, y, 3 * 32, 32);

	this.iconX = x + 5;
	this.iconY = y + 3;

	this.background.fixedToCamera = true;

	this.icon = game.add.sprite(null, null, icon.key, icon.frame);
	this.icon.fixedToCamera = true;
	this.icon.cameraOffset.x = this.iconX;
	this.icon.cameraOffset.y = this.iconY;

	this.text = game.add.text(null, null, text, this.textStyle);
	this.text.fixedToCamera = true;
	this.text.cameraOffset.x = x + 40;
	this.text.cameraOffset.y = y + 3;
}

Info.prototype.textStyle = {
	fill: "#ffffff"
};
Info.prototype.rectangleX = 1200;
Info.prototype.rectangleY = 0;
Info.prototype.rectangleHeight = 38;

Info.prototype.kill = function () {
	/*game.scheduleDestroy(this.icon);
	game.scheduleDestroy(this.text);
	this.background.empty = true;
	delete this.background;*/
};

Info.prototype.setText = function (text) {
	this.text.text = text;
};

Info.prototype.setIcon = function (icon) {
	this.icon.fixedToCamera = false;
	game.scheduleDestroy(this.icon);

	this.icon = game.add.sprite(null, null, icon.key, icon.frame);
	this.icon.fixedToCamera = true;
	this.icon.cameraOffset.x = this.iconX;
	this.icon.cameraOffset.y = this.iconY;
};

export default Info;
