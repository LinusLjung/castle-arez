var Unit = {
  hp: 100,
  damage: 25,
  invulnerable: false,
  getDamage: function () {
    return this.damage;
  },
  takeDamage: function (amount) {
    console.log(this.invulnerable);
    if (!this.invulnerable) {
      this.invulnerable = true;
      this.hp -= amount;

  		if (this.hp <= 0) {
  			this.die();
  		}

      if (typeof this.onTakeDamage === "function") {
        this.onTakeDamage();
      }
    }
  }
};

export default Unit;
