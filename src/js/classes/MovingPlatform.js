function MovingPlatform(game, x, y, width, height, sprite, frame, vertical, speed, bounds) {
	Phaser.TileSprite.call(this, game, x, y, width, height, sprite, frame);

	game.add.existing(this);

	game.physics.arcade.enable(this);
	this.body.immovable = true;

	this.vertical = vertical;
	this.speed = speed;
	this.bounds = bounds;

	if (vertical) {
		this.body.velocity.y = speed;
	} else {
		this.body.velocity.x = speed;
	}
}

MovingPlatform.prototype = Object.create(Phaser.Sprite.prototype);
MovingPlatform.prototype.constructor = MovingPlatform;

MovingPlatform.prototype.update = function () {
	if (this.vertical) {
		if (this.body.position.y < this.bounds[0]) {
			this.body.velocity.y = this.speed;
		} else if(this.body.position.y > this.bounds[1]) {
			this.body.velocity.y = -this.speed;
		}
	} else {
		if (this.body.position.x < this.bounds[0]) {
			this.body.velocity.x = this.speed;
		} else if(this.body.position.x > this.bounds[1]) {
			this.body.velocity.x = -this.speed;
		}
	}
};

export default MovingPlatform;
