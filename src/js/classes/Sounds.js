const Sounds = {};

export default {
  get: function (key) {
    return Sounds[key];
  },
  set: function (key, value) {
    Sounds[key] = value;
  }
};
