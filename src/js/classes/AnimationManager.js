function AnimationManager(object) {
	this.currentlyPlaying = null;
	this.animations = {};
	this.object = object;
}

AnimationManager.prototype.add = function (key, frames, framerate, loop, priority, onComplete, prevent) {
	var animation;

	prevent = ((typeof prevent === "undefined") ? [] : prevent);

	if (Array.isArray(frames)) {
		animation = this.object.animations.add(key, frames, framerate, loop);

		animation.onComplete.add(function () {
			this.currentlyPlaying = null;

			if (typeof onComplete === "function") {
				onComplete.call(this.object);
			}
		}, this);
	} else {
		animation = frames;
	}

	this.animations[key] = {
		animation: animation,
		priority: priority,
		prevent: prevent
	};
};

AnimationManager.prototype.play = function (key, onComplete) {
	var animation = this.animations[key];

	if (typeof animation === "undefined") {
		this.play(this.defaultAnimation);
	} else if ((this.currentlyPlaying === null || animation.priority >= this.currentlyPlaying.priority) && this.currentlyPlaying !== animation && (this.currentlyPlaying === null || this.currentlyPlaying.prevent.indexOf(key) === -1)) {
		if (typeof onComplete === "function") {
			animation.animation.onComplete.addOnce(onComplete, this.object);
		}

		if (animation.animation instanceof Phaser.Animation) {
			animation.animation.play();
		} else if (typeof animation.animation === "number" || typeof animation.animation === "string") {
			this.object.frame = animation.animation;
		}

		this.currentlyPlaying = animation;
	}
};

AnimationManager.prototype.stop = function (priorityOrKey, playDefault) {
	var animation = this.currentlyPlaying;
	var stop = false;

	if (typeof playDefault !== "boolean") {
		playDefault = false;
	}

	if (animation === null) {
		if (playDefault) {
			this.currentlyPlaying = this.play(this.defaultAnimation);
		}
	} else if (typeof priorityOrKey === "string") {
		if (animation === this.animations[priorityOrKey]) {
			stop = true;
		}
	} else if (priorityOrKey >= animation.priority) {
		if (animation.animation instanceof Phaser.Animation) {
			stop = true;
		}
	}

	if (stop) {
		if (playDefault) {
			this.currentlyPlaying = this.play(this.defaultAnimation);
		} else {
			animation.animation.stop();
			this.currentlyPlaying = null;
		}
	}
};

AnimationManager.prototype.setDefaultAnimation = function (animation) {
	this.defaultAnimation = animation;
};

export default AnimationManager;
