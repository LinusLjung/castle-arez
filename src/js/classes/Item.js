function Item(type, subType, quantity, cooldown, groupOrSprite, icon, sound) {
	this.type = type;
	this.subType = subType;
	this.quantity = (quantity === null) ? 1 : quantity;
	this.cooldown = cooldown;
	this.group = groupOrSprite;
	this.sprite = groupOrSprite;
	this.readyTime = 0;
	this.icon = icon;
	this.sound = sound;
}

Item.prototype.playSound = function () {
	if (typeof this.sound !== "undefined" && this.sound !== null) {
		this.sound.play();
	}
};

Item.prototype.decrement = function () {
	this.quantity--;

	if (typeof this.inventory !== "undefined") {
		this.inventory.updateEquipText();
	}
};

export default Item;
