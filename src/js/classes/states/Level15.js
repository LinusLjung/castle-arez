import Arez from 'Arez';
import Tiles from 'Tiles';
import Sounds from 'Sounds';
import WorldChildSaver from 'WorldChildSaver';
import TriggerArea from 'TriggerArea';
import Updater from 'Updater';
import Goblin from 'enemies/Goblin';
import Collider from 'Collider';
import ItemObject from 'ItemObject';
import Item from 'Item';
import MovingPlatform from 'MovingPlatform';
import ShootingTrap from 'ShootingTrap';
import FallTrap from 'FallTrap';

var hero;

function Level15() {
}

Level15.prototype.preload = preload;
Level15.prototype.create = create;
Level15.prototype.update = update;

function preload() {
}

function create() {
	var map;
	var layer;
	var goblins;
	var goblin;
	var movingPlatforms;
	var arrows;
	var shootingTrap;
	var ducks;
	var door;
	var finishTrigger;
	var duckObjects;
	var fallTraps;
	var fireballs;
	var fireballTraps;
	var background;

	Arez.currentLevel = Level15;

	this.background = background = game.add.tileSprite(0, 0, 0, 0, "level15background");

	game.physics.startSystem(Phaser.Physics.ARCADE);
	game.physics.arcade.checkCollision.down = false;

	map = game.add.tilemap('tilemap15');
	map.addTilesetImage('castlearez');

	map.setCollision(Tiles.dark.index);
	map.setCollision(Tiles.spear.index);
	map.setCollision(Tiles.box.index);
	map.setCollision(Tiles.lava.index);

	layer = map.createLayer('Tile Layer 1');
	layer.resizeWorld();

	background.width = game.world.width;
	background.height = game.world.height;

	hero = WorldChildSaver.restore("hero");
	ducks = WorldChildSaver.restore("ducks");

	hero.registerEvents();
	hero.newHealthInfo();

	door = game.add.sprite(104 * 32, 19 * 32, "door");

	hero.position.x = 4 * 32;
	hero.position.y = 19 * 32;
	hero.normal();
	hero.inventory.newEquipInfo(true);

	WorldChildSaver.restore("sword");

	finishTrigger = new TriggerArea(104 * 32, 	16.5 * 32, 2.66 * 32, 16);
	finishTrigger.addTriggerer(hero);
	finishTrigger.onTrigger = finish;

	Updater.add("finishTrigger", finishTrigger);

	goblins = game.add.group();

	goblin = new Goblin(game, 76 * 32, 27 * 32, "goblin");
	goblin.patrol = {
		left: 76.5 * 32,
		right: 86.5 * 32
	};
	goblins.add(goblin);

	goblin = new Goblin(game, 93 * 32, 27 * 32, "goblin");
	goblin.patrol = {
		left: 93.5 * 32,
		right: 98.5 * 32
	};
	goblins.add(goblin);

	goblin = new Goblin(game, 37 * 32, 29 * 32, "goblin");
	goblin.patrol = {
		left: 38 * 32,
		right: 39.5 * 32
	};
	goblins.add(goblin);

	Level15.goblins = goblins;

	//Lootable ducks
	duckObjects = game.add.group();
	duckObjects.classType = ItemObject;
	duckObjects.create(192, 288, "castlearez", Tiles.duckStationary.index - 1);
	duckObjects.setAll("item", new Item("weapon", "throw", 10, 20, ducks, new Phaser.Sprite(game, null, null, "castlearez"), Sounds.get("quack")), undefined, undefined, undefined, true);

	Level15.themeSound = Sounds.get("level15");
	Level15.themeSound.play();

	movingPlatforms = game.add.group();
	movingPlatforms.add(new MovingPlatform(game, 11 * 32, 22 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformRed.index - 1, true, 75, [22 * 32, 28 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 86 * 32, 20 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformRed.index - 1, true, 75, [14 * 32, 22 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 19 * 32, 28 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.movingPlatformBlue.index - 1, false, 75, [14 * 32, 22 * 32])); //At start
	movingPlatforms.add(new MovingPlatform(game, 47 * 32, 11 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformBlue.index - 1, false, 75, [44 * 32, 47 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 38 * 32, 11 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformBlue.index - 1, false, 75, [34 * 32, 42 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 29 * 32, 11 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformBlue.index - 1, false, 75, [26 * 32, 32 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 23 * 32, 11 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformBlue.index - 1, false, 75, [18 * 32, 24 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 14 * 32, 11 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformBlue.index - 1, false, 75, [11 * 32, 16 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 7 * 32, 11 * 32, 2 * 32, 1 * 32, "castlearez", Tiles.movingPlatformBlue.index - 1, false, 75, [5 * 32, 9 * 32]));
	movingPlatforms.add(new MovingPlatform(game, 0 * 32, 9 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.movingPlatformRed.index - 1, true, 75, [9 * 32, 23 * 32]));

	arrows = game.add.group();
	arrows.enableBody = true;
	arrows.physicsBodyType = Phaser.Physics.ARCADE;
	arrows.createMultiple(3, "castlearez", Tiles.arrow.index - 1);
	arrows.setAll("anchor.x", .5);
	arrows.setAll("anchor.y", .5);
	arrows.setAll("outOfBoundsKill", true);
	arrows.setAll("checkWorldBounds", true);
	arrows.setAll("direction", "left");
	arrows.setAll("body.height", 1);

	shootingTrap = game.add.existing(new ShootingTrap(game, 27 * 32, 	27 * 32, "castlearez", Tiles.box.index - 1, false, -200, 3000, new Item("trap", "ranged", 0, 0, arrows, null, null)));

	fallTraps = game.add.group();
	fallTraps.add(new FallTrap(game, 57 * 32, 27 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .125));
	fallTraps.add(new FallTrap(game, 61 * 32, 27 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .125));
	fallTraps.add(new FallTrap(game, 65 * 32, 27 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .125));

	fallTraps.add(new FallTrap(game, 83 * 32, 13 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 82 * 32, 13 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 81 * 32, 13 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 79 * 32, 11 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 78 * 32, 11 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 75 * 32, 10 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 71 * 32, 12 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 68 * 32, 11 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 65 * 32, 11 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 62 * 32, 10 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 58 * 32, 12 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));
	fallTraps.add(new FallTrap(game, 55 * 32, 11 * 32, 1 * 32, 1 * 32, "castlearez", Tiles.fallTrap.index - 1, true, .5));

	fireballs = game.add.group();
	fireballs.enableBody = true;
	fireballs.physicsBodyType = Phaser.Physics.ARCADE;
	fireballs.createMultiple(6, "castlearez", Tiles.fireball.index - 1);
	fireballs.setAll("body.gravity.y", 200);
	fireballs.setAll("anchor.x", .5);
	fireballs.setAll("anchor.y", .5);
	fireballs.setAll("direction", "up", null, null, null, true);
	fireballs.setAll("update", function () {
		if (this.body.velocity.y > 0) {
			if (this.direction !== "down") {
				this.scale.y *= -1;
				this.direction = "down";
			}
		} else if (this.body.velocity.y < 0) {
			if (this.direction !== "up") {
				this.scale.y *= -1;
				this.direction = "up";
			}
		}
	});

	fireballs.name = "fireballs";

	fireballTraps = game.add.group();
	fireballTraps.add(new ShootingTrap(game, 60 * 32, 30 * 32, "castlearez", Tiles.lava.index - 1, true, -300, 2200, new Item("trap", "ranged", 0, 0, fireballs, null, null), 1100));
	fireballTraps.add(new ShootingTrap(game, 64 * 32, 30 * 32, "castlearez", Tiles.lava.index - 1, true, -300, 2200, new Item("trap", "ranged", 0, 0, fireballs, null, null)));
	fireballTraps.setAll("anchor.x", .5);

	game.camera.follow(hero);

	Collider.clear();
	Updater.clear();

	Collider.addCollision("layerHero", {
		first: layer,
		second: hero,
		onCollision: hero.onLayerCollision,
		context: hero
	});
	Collider.addCollision("heroEnemies", {
		first: hero,
		second: goblins,
		checkCollision: hero.die,
		context: hero
	});
	Collider.addCollision("duckObjectsHero", {
		first: duckObjects,
		second: hero,
		checkCollision: hero.loot,
		context: hero
	});
	Collider.addCollision("goblinsDucks", {
		first: goblins,
		second: ducks,
		onCollision: Arez.collideGoblinProjectile
	});
	Collider.addCollision("ducksLayer", {
		first: ducks,
		second: layer,
		onCollision: Arez.collideDuckLayer
	});
	Collider.addCollision("heroMovingPlatforms", {
		first: hero,
		second: movingPlatforms
	});
	Collider.addCollision("heroArrows", {
		first: hero,
		second: arrows,
		checkCollision: hero.die,
		context: hero
	});
	Collider.addCollision("heroShootingTrap", {
		first: hero,
		second: shootingTrap
	});
	Collider.addCollision("heroFallTraps", {
		first: hero,
		second: fallTraps,
		onCollision: Arez.collideHeroFallTrap
	});
	Collider.addCollision("heroFireballs", {
		first: hero,
		second: fireballs,
		checkCollision: hero.die,
		context: hero
	});
	Collider.addCollision("fireballsFireballTraps", {
		first: fireballs,
		second: fireballTraps,
		checkCollision: Arez.collideFireballTrap
	});
}

function update() {
	game.handleScheduledDestroys();
	Updater.update();
	Collider.checkCollisions();
	this.background.tilePosition.y = game.camera.y;
}

function finish() {
	console.log("finish");
}

Arez.Level15 = Level15;

export default Level15;
