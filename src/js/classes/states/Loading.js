import Arez from 'Arez';
import Sounds from 'Sounds';
import Hero from 'Hero';
import WorldChildSaver from 'WorldChildSaver';
import Tiles from 'Tiles';
import SwingWeapon from 'SwingWeapon';

function Loading() {
}

Loading.prototype.preload = function () {
	var loadingText = game.add.text(game.width / 2, game.height / 2, 'Loading...', { font: '30px Arial', fill: '#fff' });

	loadingText.anchor.setTo(0.5, 0.5);
	//game.stage.backgroundColor = "#611515";

	game.load.spritesheet('castlearez', 'assets/tilesets/castlearez.png', 32, 32);

	//game.load.spritesheet('hero', 'assets/sprites/hero.png', 28, 51);
	game.load.atlasJSONArray("tim", "assets/sprites/tim-atlas.png", "assets/sprites/tim-atlas.json");
	//game.load.atlasJSONArray("dragon", "assets/sprites/dragon-atlas.png", "assets/sprites/dragon-atlas.json");
	game.load.atlasJSONArray("bat", "assets/sprites/bat-atlas.png", "assets/sprites/bat-atlas.json");
	game.load.spritesheet('goblin', 'assets/sprites/goblin.png', 32, 32, 12);
	game.load.spritesheet('ghost', 'assets/sprites/ghost.png', 32, 32);
	game.load.spritesheet('blood', 'assets/sprites/blood.png', 15, 20);
	game.load.spritesheet("torch", "assets/sprites/torch.png", 55/3, 58);

	game.load.tilemap('tilemap1', 'assets/tilemaps/castle1.json', null, Phaser.Tilemap.TILED_JSON);
	// game.load.tilemap('tilemap2', 'assets/tilemaps/castle2.json', null, Phaser.Tilemap.TILED_JSON);
	// game.load.tilemap('tilemap3', 'assets/tilemaps/castle3.json', null, Phaser.Tilemap.TILED_JSON);
	// game.load.tilemap('tilemap4', 'assets/tilemaps/castle4.json', null, Phaser.Tilemap.TILED_JSON);
	/*game.load.tilemap('tilemap5', 'assets/tilemaps/castle5.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap6', 'assets/tilemaps/castle6.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap7', 'assets/tilemaps/castle7.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap8', 'assets/tilemaps/castle8.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap9', 'assets/tilemaps/castle9.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap10', 'assets/tilemaps/castle10.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap11', 'assets/tilemaps/castle11.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap12', 'assets/tilemaps/castle12.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap13', 'assets/tilemaps/castle13.json', null, Phaser.Tilemap.TILED_JSON);
	game.load.tilemap('tilemap14', 'assets/tilemaps/castle14.json', null, Phaser.Tilemap.TILED_JSON);*/
	game.load.tilemap('tilemap15', 'assets/tilemaps/castle15.json', null, Phaser.Tilemap.TILED_JSON);

	game.load.image('door', 'assets/sprites/CastleDoor1.png');
	game.load.image("log", "assets/sprites/log.png");
	game.load.image("invisible", "assets/sprites/invisible.png");

	game.load.image("level1background", "assets/sprites/level1background.png");
	// game.load.image("level2background", "assets/sprites/level2background.png");
	game.load.image("level15background", "assets/sprites/level15background.png");

	game.load.audio("theme-level1", ["assets/sounds/level-themes/level1.ogg"]);
	game.load.audio("theme-level15", ["assets/sounds/level-themes/level15.ogg"]);
	game.load.audio("wilhelm", ["assets/sounds/wilhelm.ogg"]);
	game.load.audio("quack", ["assets/sounds/quack.ogg"]);
	game.load.audio("goblin-dies", ["assets/sounds/goblin-dies.ogg"]);
	game.load.audio("loot", ["assets/sounds/loot.ogg"]);
	game.load.audio("victory", ["assets/sounds/victory.ogg"]);
	game.load.audio("secret-sound", ["assets/sounds/secret.ogg"]);
	game.load.audio("gameover", ["assets/sounds/gameover.ogg"]);
	game.load.audio("swoosh", ["assets/sounds/swoosh.ogg"]);
	game.load.audio("ouch", ["assets/sounds/ouch.ogg"]);
	game.load.audio("break-box", ["assets/sounds/break-box.ogg"]);
};

Loading.prototype.create = function () {
	var themeLevel1 = game.add.audio('theme-level1', .2, true);
	var themeLevel15 = game.add.audio('theme-level15', .2, true);
	var sword;

	Arez.cursors = game.input.keyboard.createCursorKeys();
	Arez.cursors.w = game.input.keyboard.addKey(Phaser.Keyboard.W);
	Arez.cursors.a = game.input.keyboard.addKey(Phaser.Keyboard.A);
	Arez.cursors.s = game.input.keyboard.addKey(Phaser.Keyboard.S);
	Arez.cursors.d = game.input.keyboard.addKey(Phaser.Keyboard.D);
	Arez.keys = {};
	Arez.keys.spacebar = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
	Arez.keys.q = game.input.keyboard.addKey(Phaser.Keyboard.Q);
	Arez.keys.e = game.input.keyboard.addKey(Phaser.Keyboard.E);

	Sounds.set("level1", themeLevel1);
	Sounds.set("level15", themeLevel15);
	Sounds.set("wilhelm", game.add.audio('wilhelm', .1));
	Sounds.set("quack", game.add.audio("quack", .1));
	Sounds.set("goblinDies", game.add.audio("goblin-dies", .1));
	Sounds.set("loot", game.add.audio("loot", .1));
	Sounds.set("victory", game.add.audio("victory", .1));
	Sounds.set("secret", game.add.audio("secret-sound", .1));
	Sounds.set("swoosh", game.add.audio("swoosh", .1));
	Sounds.set("ouch", game.add.audio("ouch", .1));
	Sounds.set("breakBox", game.add.audio("break-box", .4));

	Sounds.get("loot").onStop.add(function () {
		Arez.currentLevel.themeSound.resume();
	});

	Sounds.get("secret").onStop.add(function () {
		Arez.currentLevel.themeSound.resume();
	});

	Arez.hero = new Hero(game, 2 * 32, 13 * 32, "tim", "idle1");
	WorldChildSaver.add("hero", Arez.hero);

	game.input.gamepad.start();

	Arez.gamepad = game.input.gamepad.pad1;

	Arez.gamepad.callbackContext = Arez.hero;

	Arez.gamepad.addCallbacks(this, {
		onConnect: Arez.hero.registerGamepad,
	});

	sword = new SwingWeapon(game, 0, 0, "castlearez", Tiles.sword.index - 1);
	game.physics.arcade.enable(sword);
	sword.anchor = new Phaser.Point(0, 1);
	sword.visible = false;
	sword.angle = 270;
	sword.direction = "right";

	WorldChildSaver.add("sword", sword);

	//Duck projectile group
	Arez.ducks = game.add.group();
	Arez.ducks.enableBody = true;
	Arez.ducks.physicsBodyType = Phaser.Physics.ARCADE;
	Arez.ducks.createMultiple(3, "castlearez", Tiles.duckProjectile.index - 1);
	Arez.ducks.setAll("anchor.x", 0.5);
	Arez.ducks.setAll("anchor.y", 1);
	Arez.ducks.setAll('outOfBoundsKill', true);
	Arez.ducks.setAll('checkWorldBounds', true);
	Arez.ducks.setAll("direction", "right", undefined, undefined, undefined, true);

	WorldChildSaver.add("ducks", Arez.ducks);

	WorldChildSaver.saveAll();

	themeLevel1.onDecoded.add(function () {
		game.state.start("level1");
	});
};

export default Loading;
