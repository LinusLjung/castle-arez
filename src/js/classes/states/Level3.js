(function (window, document, undefined) {
	var numberOfDucks = 0;
	var text;
	var	hero;
	var layer;
	var duckObjects;
	var duckItems;
	var goblin;
	var log;
	var log2;

	function Level3() {
	}

	Level3.prototype.preload = preload;
	Level3.prototype.create = create;
	Level3.prototype.update = update;

	function preload() {
	}

	function create() {
		var map;

		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.physics.arcade.checkCollision.down = false;

		window.sounds = {
			theme: game.add.audio('theme-song', .25),
			wilhelm: game.add.audio('wilhelm', .25),
			quack: game.add.audio("quack", .25),
			goblinDies: game.add.audio("goblin-dies", .25),
			loot: game.add.audio("loot", .25),
			victory: game.add.audio("victory", .25),
			secret: game.add.audio("secret-sound", .25)
		};


		map = game.add.tilemap('tilemap3');
		map.addTilesetImage('castlearez');

		map.setCollision(tiles.grass);
		map.setCollision(tiles.water);
		map.setCollision(tiles.grey);
		map.setCollision(tiles.cobblestone);
		map.setCollision(tiles.purpleLeaf);
		map.setCollision(tiles.hidden);
		map.setCollision(tiles.lava);
		map.setCollision(tiles.tree);
		map.setCollision(tiles.secret);

		layer = map.createLayer('Tile Layer 1');
		layer.resizeWorld();

		window.cursors = game.input.keyboard.createCursorKeys();

		door = game.add.sprite(69 * 32, 22 * 32, "door");
		door.anchor.y = 1;
		game.physics.arcade.enable(door);


		//Duck projectile group
		ducks = game.add.group();
		ducks.enableBody = true;
		ducks.physicsBodyType = Phaser.Physics.ARCADE;
		ducks.createMultiple(3, "duck");
		ducks.setAll("anchor.x", 0.5);
		ducks.setAll("anchor.y", 1);
		ducks.setAll('outOfBoundsKill', true);
		ducks.setAll('checkWorldBounds', true);
		ducks.setAll("direction", "right", undefined, undefined, undefined, true);

		duckTime = 0;

		duckObjects = game.add.group();
		duckObjects.classType = ItemObject;
		duckObjects.create(5 * 32, 20 * 32, "duck1");
		duckObjects.setAll("item", new Item("weapon", "throw", 10, 20, ducks, new Phaser.Sprite(game, null, null, "duckIcon"), sounds.quack), undefined, undefined, undefined, true);

		sounds.loot.onStop.add(function () {
			sounds.theme.resume();
		});

		sounds.secret.onStop.add(function () {
			sounds.theme.resume();
		});


		sounds.theme.play();
		logs = game.add.group();
		log = new FallTrap(game, 20 * 32, 20 * 32, "log", true, 0);
		logs.add(log);
		log = new FallTrap(game, 47 * 32, 19 * 32, "log", true, 0);
		logs.add(log);

		goblins = game.add.group();

		goblin = new Goblin(game, 20 * 32, 20 * 32, "goblin");
		goblin.patrol = {
			left: 19.5 * 32,
			right: 24.5 * 32
		};
		goblins.add(goblin);

		goblin = new Goblin(game, 39 * 32, 23 * 32, "goblin");
		goblin.patrol = {
			left: 40 * 32,
			right: 42 * 32
		};
		goblins.add(goblin);

		goblin = new Goblin(game, 65 * 32, 6 * 32, "goblin");
		goblin.patrol = {
			left: 65.5 * 32,
			right: 69.5 * 32
		};
		goblins.add(goblin);
		/*animateTorch = [0, 1, 2, 1];

		torch = game.add.sprite(16*32, 7*32, "torch");
		torch.animations.add("blow", animateTorch, 4, true);
		torch.animations.play("blow");

		torch = game.add.sprite(58*32, 9*32 - 16, "torch");
		torch.animations.add("blow", animateTorch, 4, true);
		torch.animations.play("blow");

		torch = game.add.sprite(54*32, 9*32 - 16, "torch");
		torch.animations.add("blow", animateTorch, 4, true);
		torch.animations.play("blow");*/

		hero = new Hero(game, 4 * 32, 19 * 32, "hero");
		game.add.existing(hero);
		game.camera.follow(hero);
		/*hero = new Hero(game, null, null, "hero");
		hero.position.x = 4 * 32;
		hero.position.y = 21 * 32;*/
	}

	function update() {
		game.handleScheduledDestroys();

		game.physics.arcade.collide(layer, hero, hero.onLayerCollide, null, hero);
		game.physics.arcade.collide(hero, goblins, hero.die);
		game.physics.arcade.collide(duckObjects, hero, hero.loot, null, hero);
		game.physics.arcade.collide(goblins, ducks, collideGoblinProjectile);
		game.physics.arcade.collide(logs, hero, collideHeroLogTrap);
		game.physics.arcade.collide(ducks, layer, collideDuckLayer);
		game.physics.arcade.collide(hero, door, null, collideHeroDoor);

		if (typeof arez !== "undefined") {
			game.physics.arcade.collide(hero, arez, arezGameOver);
		}
	}

	function collideDuckLayer(duck, tile) {
		duck.kill();

		if (tile.index === tiles.secret) {
			/*tile.layer.data[14][0].faceBottom = true;
			//tile.layer.data[15][0].alpha = 0;
			tile.layer.data[15][0].index = 2;
			tile.layer.data[15][0].collideDown = false;
			tile.layer.data[15][0].collideUp = false;
			tile.layer.data[15][0].collideRight = false;
			tile.layer.data[15][0].collideLeft = false;
			//tile.layer.data[16][0].alpha = 0;
			tile.layer.data[16][0].index = 2;
			tile.layer.data[16][0].collideDown = false;
			tile.layer.data[16][0].collideUp = false;
			tile.layer.data[16][0].collideRight = false;
			tile.layer.data[16][0].collideLeft = false;
			//tile.layer.data[17][0].collideUp = true;
			tile.layer.data[17][0].faceTop = true;*/

			for (var y = 0; y < tile.layer.data.length; y++) {
				for (var x = 0; x < tile.layer.data[y].length; x++) {
					if (tile.layer.data[y][x].index === tiles.secret) {
						tile.layer.data[y][x].alpha = 0;
						tile.layer.data[y][x].collideDown = false;
						tile.layer.data[y][x].collideUp = false;
						tile.layer.data[y][x].collideRight = false;
						tile.layer.data[y][x].collideLeft = false;
					}
				}
			}

			tile.layer.dirty = true;

			arez = game.add.sprite(65 * 32, 22 * 32, "arez");
			game.physics.arcade.enable(arez);
			arez.enableBody = true;
			arez.anchor.y = 1;

			sounds.theme.pause();
			sounds.secret.play();
		}

		blood(duck);
	}

	function arezGameOver() {
		sounds.theme.stop();
		sounds.wilhelm.play();
		game.state.start("over");
	}

	function blood(unit) {
		new Blood(unit);
	}

	/*function goblinDies(goblin, duck) {
		ghost = game.add.sprite(goblin.position.x, goblin.position.y, "ghost");
		blood(goblin);
		duck.kill();
		sounds.goblinDies.play();
	}*/

	function collideHeroDoor(hero, door) {
		if (hero.position.x > door.position.x + 2 * 32 && hero.body.onFloor()) {
			hero.destroy();

			sounds.theme.stop();
			sounds.victory.play();
			game.state.start("level4");

			hero.position.y = 19 * 32;
			hero.position.x = 4 * 32;
		}

		return false;
	}

	function collideGoblinProjectile(goblin, projectile) {
		projectile.kill();
		goblin.die();
	}

	function collideHeroLogTrap(hero, log) {
		log.fall();
	}

	window.Level3 = Level3;
})(window, document);
