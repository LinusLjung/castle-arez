import Arez from 'Arez';
import Sounds from 'Sounds';
import Tiles from 'Tiles';
import WorldChildSaver from 'WorldChildSaver';
import Bat from 'enemies/Bat';
import Goblin from 'enemies/Goblin';
import Spawner from 'Spawner';
import TriggerArea from 'TriggerArea';
import FallTrap from 'FallTrap';
import Updater from 'Updater';
import Ladder from 'Ladder';
import ItemObject from 'ItemObject';
import Item from 'Item';
import Destroyable from 'Destroyable';
import Collider from 'Collider';
import Blood from 'Blood';
import Force from 'Force';

var hero;
var log;

function Level1() {
}

Level1.prototype.preload = preload;
Level1.prototype.create = create;
Level1.prototype.update = update;
Level1.prototype.shutdown = shutdown;

function preload() {
}

function create() {
	var map;
	var layer1;
	var layer2;
	var secret;
	var background;
	var finishTrigger;
	var flyTrigger;
	var door;
	var duckObjects;
	var ducks;
	var goblins;
	var goblin;
	var torch;
	var animateTorch;
	var sword;
	var swordObject;
	var swordSprite;
	var bats;
	var batSpawner;
	var enemies;

	Arez.currentLevel = Level1;

	Arez.currentLevel.destroyables = [];

	game.physics.startSystem(Phaser.Physics.ARCADE);
	game.physics.arcade.checkCollision.down = false;

	map = game.add.tilemap('tilemap1');
	map.addTilesetImage('castlearez');

	map.setCollision(Tiles.dark.index);
	map.setCollision(Tiles.lava.index);
	map.setCollision(Tiles.box.index);
	map.setCollision(Tiles.speard.index);
	map.setCollision(Tiles.grey.index);
	map.setCollision(Tiles.log1.index);
	map.setCollision(Tiles.log2.index);
	map.setCollision(Tiles.log3.index);
	map.setCollision(Tiles.log4.index);

	background = game.add.tileSprite(0, 0, 0, 0, "level1background");

	layer1 = map.createLayer('Tile Layer 1');
	layer1.resizeWorld();

	layer2 = map.createLayer("Tile Layer 2");

	background.width = game.world.width;
	background.height = game.world.height;

	Level1.themeSound = Sounds.get("level1");

	hero = WorldChildSaver.restore("hero");
	ducks = WorldChildSaver.restore("ducks");

	hero.registerEvents();
	hero.newHealthInfo();

	game.camera.follow(hero);

	bats = game.add.group();
	bats.physicsBodyType = Phaser.Physics.ARCADE;
	bats.enableBody = true;
	bats.classType = Bat;
	bats.createMultiple(5, "bat", "flying1");
	bats.forEach(function (bat) {
		bat.setX(game.world.width + 2 * 32);
		bat.setY(9 * 32);
	});
	game.add.existing(bats);

	batSpawner = new Spawner(bats, null, null, 3000, 0);

	door = game.add.sprite(52 * 32, 10 * 32, "door");

	finishTrigger = new TriggerArea(53.625 * 32, 	16.5 * 32, 2.66 * 32, 16);
	finishTrigger.addTriggerer(hero);
	finishTrigger.onTrigger = finish;

	Updater.add("finishTrigger", finishTrigger);

	log = game.add.existing(new FallTrap(game, 25 * 32, 17 * 32, 4 * 32, 1 * 32, "log", null, false, 1000));

	Arez.ladders = game.add.group();
	Arez.ladders.add(new Ladder(game, 3.2 * 32, 17 * 32, 32 * 2, 10 * 32, 2));

	goblins = game.add.group();

	goblin = new Goblin(game, 28 * 32, 9 * 32, "goblin");
	goblin.patrol = {
		left: 28.5 * 32,
		right: 31.5 * 32
	};
	goblins.add(goblin);

	goblin = new Goblin(game, 29 * 32, 17 * 32, "goblin");
	goblin.patrol = {
		left: 29.5 * 32,
		right: 36.5 * 32
	};
	goblins.add(goblin);

	goblin = new Goblin(game, 48 * 32, 17 * 32, "goblin");
	goblin.patrol = {
		left: 48.5 * 32,
		right: 53.5 * 32
	};
	goblins.add(goblin);

	enemies = [bats, goblins];

	//Lootable ducks
	duckObjects = game.add.group();
	duckObjects.classType = ItemObject;
	duckObjects.add(new ItemObject(game, 6 * 32, 8.5 * 32, "castlearez", Tiles.duckStationary.index - 1));
	duckObjects.setAll("item", new Item("weapon", "throw", 10, 20, ducks, new Phaser.Sprite(game, null, null, "castlearez", Tiles.duckIcon.index - 1), Sounds.get("quack")), undefined, undefined, undefined, true);

	swordSprite = WorldChildSaver.restore("sword");

	sword = new Item("weapon", "melee", null, 10, swordSprite, new Phaser.Sprite(game, 0, 0, "castlearez", Tiles.sword.index - 1), Sounds.get("quack"));
	sword.anchor = new Phaser.Point(0, 1);

	swordObject = game.add.existing(new ItemObject(game, 5 * 32, 13 * 32, "castlearez", Tiles.sword.index - 1));
	swordObject.item = sword;

	animateTorch = [0, 1, 2, 1];

	torch = game.add.sprite(16 * 32, 7 * 32, "torch");
	torch.animations.add("blow", animateTorch, 4, true);
	torch.animations.play("blow");

	torch = game.add.sprite(57 * 32, 9 * 32 - 16, "torch");
	torch.animations.add("blow", animateTorch, 4, true);
	torch.animations.play("blow");

	torch = game.add.sprite(52 * 32, 9 * 32 - 16, "torch");
	torch.animations.add("blow", animateTorch, 4, true);
	torch.animations.play("blow");

	/*secret = new Destroyable([
		{
			x: 0,
			y: 15,
			index: Tiles.dark.index
		},
		{
			x: 0,
			y: 16,
			index: Tiles.dark.index
		}
	], ducks);

	secret.onDestroy = collideDuckSecret;*/

	Arez.currentLevel.destroyables.push(game.add.existing(new Destroyable(15 * 32, 13 * 32, [Tiles.box.index - 1, Tiles.boxCracked.index - 1], "breakBox")));
	Arez.currentLevel.destroyables.push(game.add.existing(new Destroyable(16 * 32, 12 * 32, [Tiles.box.index - 1, Tiles.boxCracked.index - 1], "breakBox")));
	Arez.currentLevel.destroyables.push(game.add.existing(new Destroyable(17 * 32, 12 * 32, [Tiles.box.index - 1, Tiles.boxCracked.index - 1], "breakBox")));
	Arez.currentLevel.destroyables.push(game.add.existing(new Destroyable(18 * 32, 12 * 32, [Tiles.box.index - 1, Tiles.boxCracked.index - 1], "breakBox")));
	Arez.currentLevel.destroyables.push(game.add.existing(new Destroyable(19 * 32, 13 * 32, [Tiles.box.index - 1, Tiles.boxCracked.index - 1], "breakBox")));

	flyTrigger = new TriggerArea(8 * 32, 16.5 * 32, 32, 16);
	flyTrigger.addTriggerer(hero);
	flyTrigger.force = new Force(750, 40);
	flyTrigger.onTrigger = function (triggerer) {
		triggerer.body.position.y -= 1;
		triggerer.body.velocity.x = this.force.x;
		triggerer.body.velocity.y = this.force.y;
		triggerer.fly();
	};

	Updater.add("flyTrigger", flyTrigger);

	game.world.bringToTop(ducks);
	game.world.bringToTop(hero);
	game.world.bringToTop(bats);
	game.world.bringToTop(layer2);

	Level1.themeSound.play();

	Collider.addCollision("layer1Hero", {
		first: layer1,
		second: hero,
		onCollision: hero.onLayerCollision,
		context: hero
	});
	Collider.addCollision("heroEnemies", {
		first: hero,
		second: enemies,
		checkCollision: Arez.collideHeroEnemy
	});
	Collider.addCollision("goblinsDucks", {
		first: goblins,
		second: ducks,
		onCollision: collideGoblinProjectile
	});
	Collider.addCollision("ducksLayer", {
		first: ducks,
		second: layer1,
		onCollision: collideDuckLayer
	});
	Collider.addCollision("logHero", {
		first: log,
		second: hero,
		onCollision: log.fall,
		context: log
	});
	Collider.addCollision("swordGoblins", {
		first: swordSprite,
		second: goblins,
		checkCollision: Arez.collideSwordGoblin
	});
}

function update() {
	game.handleScheduledDestroys();

	Updater.update();

	Collider.checkCollisions();
}

function shutdown() {
	WorldChildSaver.saveAll();
}

function collideDuckSecret(duck) {
	var arez;

	new Blood(duck);
	duck.kill();

	arez = game.add.existing(new ItemObject(game, 0, 17 * 32, "castlearez", Tiles.arez.index - 1));
	game.physics.arcade.enable(arez);
	arez.anchor.y = 1;

	Level1.themeSound.pause();
	Sounds.get("secret").play();

	Collider.addCollision("arez", {
		first: hero,
		second: arez,
		onCollision: arezGameOver
	});
}

function collideDuckLayer(duck) {
	new Blood(duck);
	duck.kill();
}

function arezGameOver() {
	Level1.themeSound.stop();
	Sounds.get("wilhelm").play();
	game.state.start("over");
}

function finish() {
	const victorySound = Sounds.get("victory");

	Updater.remove("finishTrigger");
	hero.visible = false;
	hero.unregisterEvents();
	Collider.removeCollision("heroEnemies");

	Level1.themeSound.stop();
	victorySound.play();
	victorySound.onStop.addOnce(function () {
		game.state.start("level15");
	});
}

function collideGoblinProjectile(goblin, projectile) {
	projectile.kill();
	goblin.die();
}

export default Level1;
