define(["Arez", "Tiles", "Sounds", "WorldChildSaver", "TriggerArea", "Updater", "Goblin", "Collider"], function (Arez, Tiles, Sounds, WorldChildSaver, TriggerArea, Updater, Goblin, Collider) {
	"use strict";

	var hero;

	function Level2() {
	}

	Level2.prototype.preload = preload;
	Level2.prototype.create = create;
	Level2.prototype.update = update;

	function preload() {
	}

	function create() {
		var map;
		var layer;
		var door;
		var goblins;
		var goblin;
		var ducks;
		var duckObjects;
		var arez;
		var background;
		var finishTrigger;

		Arez.currentLevel = Level2;

		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.physics.arcade.checkCollision.down = false;

		map = game.add.tilemap('tilemap2');
		map.addTilesetImage('castlearez');

		map.setCollision(Tiles.grass.index);
		map.setCollision(Tiles.water.index);
		map.setCollision(Tiles.grey.index);
		map.setCollision(Tiles.hidden.index);

		background = game.add.tileSprite(0, 0, 0, 0, "level2background");

		layer = map.createLayer('Tile Layer 1');
		layer.resizeWorld();

		background.width = game.world.width;
		background.height = game.world.height;

		door = game.add.sprite(47 * 32, 23 * 32, "door");
		door.anchor.y = 1;

		hero = WorldChildSaver.restore("hero");
		hero.position.x = 2 * 32;
		hero.position.y = 13 * 32;
		hero.normal();
		hero.inventory.newEquipInfo(true);
		hero.newHealthInfo();

		ducks = WorldChildSaver.restore("ducks");

		WorldChildSaver.restore("sword");

		finishTrigger = new TriggerArea(48.625 * 32, 22.5 * 32, 2.66 * 32, 16);
		finishTrigger.addTriggerer(hero);
		finishTrigger.onTrigger = finish;

		Updater.add("finishTrigger", finishTrigger);

		goblins = game.add.group();

		goblin = new Goblin(game, 33 * 32, 20 * 32, "goblin");
		goblin.patrol = {
			left: 33.5 * 32,
			right: 36.5 * 32
		};
		goblins.add(goblin);

		goblin = new Goblin(game, 14 * 32, 24 * 32, "goblin");
		goblin.patrol = {
			left: 14.5 * 32,
			right: 19.5 * 32
		};
		goblins.add(goblin);

		Level2.goblins = goblins;

		Level2.themeSound = Sounds.get("level1");
		Level2.themeSound.play();

		hero.position.y = 21 * 32;
		hero.position.x = 4 * 32;

		game.camera.follow(hero);

		arez = game.add.sprite(13 * 32, 8 * 32, "castlearez", Tiles.arez.index - 1);
		game.physics.arcade.enable(arez);
		arez.anchor.y = 1;

		Collider.clear();
		Updater.clear();

		Collider.addCollision("layerHero", {
			first: layer,
			second: hero,
			onCollision: hero.onLayerCollision,
			context: hero
		});
		Collider.addCollision("heroEnemies", {
			first: hero,
			second: goblins,
			checkCollision: hero.die,
			context: hero
		});
		Collider.addCollision("duckObjectsHero", {
			first: duckObjects,
			second: hero,
			checkCollision: hero.loot,
			context: hero
		});
		Collider.addCollision("goblinsDucks", {
			first: goblins,
			second: ducks,
			onCollision: Arez.collideGoblinProjectile
		});
		Collider.addCollision("ducksLayer", {
			first: ducks,
			second: layer,
			onCollision: Arez.collideDuckLayer
		});
		Collider.addCollision("arez", {
			first: hero,
			second: arez,
			onCollision: Arez.arezGameOver
		});
	}


	function update() {
		game.handleScheduledDestroys();
		Updater.update();
		Collider.checkCollisions();
	}

	function finish() {
		Updater.remove("finishTrigger");
		hero.visible = false;
		hero.unregisterEvents();
		Collider.removeCollision("heroEnemies");

		Level2.themeSound.stop();
		//Sounds.get("victory").play();
		//Sounds.get("victory").onStop.addOnce(function () {
			game.state.start("level15");
		//});
	}

	return Level2;
});
