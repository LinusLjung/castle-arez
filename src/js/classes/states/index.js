import _Loading from './Loading';
import _Level1 from './Level1';
import _Level15 from './Level15';
import _Over from './Over';

export const Loading = _Loading;
export const Level1 = _Level1;
export const Level15 = _Level15;
export const Over = Over;
