function Ghost(unit) {
	Phaser.Sprite.call(this, game, unit.position.x, unit.position.y, "ghost");

	game.physics.arcade.enable(this);
	this.checkWorldBounds = true;
	//this.events.onOutOfBounds.add(this.die.bind(this));
	this.body.velocity.y = -50;
	this.anchor = unit.anchor;
}

Ghost.prototype = Object.create(Phaser.Sprite.prototype);
Ghost.prototype.constructor = Ghost;

Ghost.prototype.update = function () {
};

Ghost.prototype.die = function () {
	game.scheduleDestroy(this);
};

export default Ghost;
