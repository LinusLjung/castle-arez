import Collider from 'Collider';
import Arez from 'Arez';

function ItemObject(game, x, y, sprite, frame) {
	Phaser.Sprite.call(this, game, x, y, sprite, frame);

	this.id = this.constructor.name + Math.floor(Date.now() * Math.random()).toString();

	Collider.addCollision("itemObject" + this.id, {
		first: this,
		second: Arez.hero,
		checkCollision: Arez.hero.loot,
		context: Arez.hero
	});

	this.events.onDestroy.add(function () {
		Collider.removeCollision(this.id);
	}, this);

	game.physics.arcade.enable(this);

	game.add.tween(this).to({
		y: this.position.y - 16
	}, 3000, Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
}

ItemObject.prototype = Object.create(Phaser.Sprite.prototype);
ItemObject.prototype.constructor = ItemObject;

ItemObject.prototype.getLoot = function () {
	return this.item;
};

export default ItemObject;
