var WorldChildSaver = {
	children: {},

	add: function (key, child) {
		WorldChildSaver.children[key] = child;
	},

	remove: function (key) {
		delete WorldChildSaver.children[key];
	},

	restore: function (key) {
		return game.add.existing(WorldChildSaver.children[key]);
	},

	restoreAll: function () {
		var key;

		for (key in WorldChildSaver.children) {
			if (WorldChildSaver.children.hasOwnProperty(key)) {
				game.add.existing(WorldChildSaver.children[key]);
			}
		}
	},

	saveAll: function () {
		var key;

		for (key in WorldChildSaver.children) {
			if (WorldChildSaver.children.hasOwnProperty(key)) {
				game.world.remove(WorldChildSaver.children[key]);
			}
		}
	},
};

export default WorldChildSaver;
