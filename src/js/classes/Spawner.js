function Spawner(spriteOrGroup, x, y, frequency, delay) {
	this.spriteOrGroup = spriteOrGroup;
	this.x = x;
	this.y = y;

	if (typeof delay === "undefined") {
		delay = 0;
	}

	if (typeof frequency === "undefined" || frequency === null || frequency <= 0) {
		window.setTimeout(function () {
			this.spawn();
		}.bind(this), delay);
	} else {
		window.setTimeout(function () {
			this.spawn();
			window.setInterval(function () {
				this.spawn();
			}.bind(this), frequency);
		}.bind(this), delay);
	}
}

Spawner.prototype.constructor = Spawner;

Spawner.prototype.spawn = function () {
	var child;

	if (this.spriteOrGroup instanceof Phaser.Group) {
		child = this.spriteOrGroup.getFirstExists(false);

		if (child !== null) {
			if (this.x !== null && this.y !== null) {
				child.reset(this.x, this.y);
			}

			if (typeof child.onSpawn === "function") {
				child.onSpawn();
			}
		}
	}
};

export default Spawner;
