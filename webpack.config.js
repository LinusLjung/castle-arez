module.exports = {
  context: __dirname + "/src",
  entry: "./js",
  output: {
    path: __dirname + "/build/js",
    filename: "js.js"
  },
  resolve: {
    root: [
      __dirname + '/src/js/classes',
      __dirname + '/src/js/lib'
    ]
  },
  externals: {
    phaser: 'Phaser'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      }
    ]
  }
};
